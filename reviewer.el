;;; reviewer -- Do reviews

;;; Commentary:


;;; Code:

(defvar reviewer-active-review)
(setq reviewer-active-review nil)


(defvar reviewer-reviews-file)
(setq reviewer-reviews-file "~/.emacs.d/reviewer.ongoing")




(defun reviewer-start-review ()
  "Make an ongoing review active or create a new one."
  (interactive)
  (let* ((ongoing-reviews (reviewer-load-ongoing-reviews))
         (review (completing-read "Select review: " ongoing-reviews)))
    (setq reviewer-active-review (assoc review ongoing-reviews))
    (unless reviewer-active-review
      (setq reviewer-active-review (cons review (read-directory-name "Review report location: ")))
      (add-to-list 'ongoing-reviews reviewer-active-review)
      (reviewer-store-ongoing-reviews ongoing-reviews))
    ))


(defun reviewer-stop-review ()
  "Deselect the active review."
  (interactive)
  (setq reviewer-active-review nil))


(defun reviewer-close-review ()
  "Remove one review from the ongoing review list."
  (interactive)
  (let* ((ongoing-reviews (reviewer-load-ongoing-reviews))
         (review (assoc
                  (completing-read "Select review to close: " ongoing-reviews nil t)
                  ongoing-reviews)))
    (when (equal review reviewer-active-review)
      (reviewer-stop-review))
    (reviewer-store-ongoing-reviews (remove review ongoing-reviews))))


(defun reviewer-close-all-reviews ()
  "Remove all reviews from the ongoing review list."
  (interactive)
  (when (yes-or-no-p "Close all ongoing reviews? ")
    (reviewer-stop-review)
    (reviewer-store-ongoing-reviews nil)))


(defun reviewer-add-finding ()
  "Add a finding to the active review."
  (interactive)
  ;; Just temporary solution
  (if reviewer-active-review
      (let ((line (line-number-at-pos))
            (file buffer-file-name)
            (finding (read-from-minibuffer "Finding: ")))
        (with-temp-buffer
          ;; maybe replace this with (insert-file-contents ...)
          (set-visited-file-name (reviewer-report-name reviewer-active-review) t)
          (if (file-exists-p buffer-file-name)
              (revert-buffer nil t)
            (save-buffer))

          (goto-char (point-max))
          (prin1 `(:file ,file :line ,line :finding ,finding) (current-buffer))
          (save-buffer))
        )
    (message "No review active!")
    )
  )



(defun reviewer-load-ongoing-reviews ()
  "Load an alist of all ongoing reviews from a file."
  (if (file-exists-p reviewer-reviews-file)
      (with-temp-buffer
        (insert-file-contents reviewer-reviews-file)
        (goto-char (point-min))
        (read (current-buffer)))
    nil))


(defun reviewer-store-ongoing-reviews (ongoing-reviews)
  "Write an alist of all ongoing reviews back to a file.
ONGOING-REVIEWS contains the alist to write."
    (with-temp-buffer
      (prin1 ongoing-reviews (current-buffer))
      (write-file reviewer-reviews-file)))


(defun reviewer-report-name (review)
  "Build the report file path from review name and location.
REVIEW is an entry from the ongoing reviews list in the form of (review-name . review-location)"
  (if review
      (concat (cdr review) (car review) "." "txt")
    nil))


(provide 'reviewer)
;;; reviewer.el ends here
